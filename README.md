# cv

Un CV très simple écrit en [Dart](https://www.dart.dev/) avec l'aide de [Flutter](https://www.flutter.dev/).

Il est techniquement possible de le compiler dans toutes les plateformes cibles, mais le projet a été initialement développé pour le web.

## Installation

1. Cloner ce dépôt.
2. Modifier les informations dans assets/data/ par vos informations.
3. Compiler l'application avec la commande
> flutter build web

## Informations

Pour compiler l'application sur Android ou ailleurs, il suffit d'aller voir la documentation officielle de flutter : https://docs.flutter.dev/

## Licence

LICENCE PUBLIQUE DE L'UNION EUROPÉENNE v. 1.2