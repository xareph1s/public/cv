import 'package:freezed_annotation/freezed_annotation.dart';

part 'either_type.freezed.dart';

@freezed
class Either<L, R> with _$Either<L, R> {
  const factory Either.left(L left) = Left<L, R>;
  const factory Either.right(R right) = Right<L, R>;
}
