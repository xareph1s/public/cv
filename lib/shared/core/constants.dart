class Constants {
  static String appName = "CV";
  static env currentEnv = env.PROD;
  static String appVersion = "1.2.0";
  static String appCodeUrl = "https://gitlab.com/xareph1s/public/cv";

  static int maxContainerSize = 1920 ~/ 1.5;

  static String contactDataLocation = 'assets/data/contacts.json';
  static String experienceDataLocation = 'assets/data/experiences.json';
  static String skillDataLocation = 'assets/data/skills.json';
  static String legalDataLocation = 'assets/data/legals.json';

  static List<String> menuList = [
    "Accueil",
    "Expériences",
    "Compétences",
    "Contacts",
    "Mentions légales",
  ];

  static List<String> pathUrl = [
    '/',
    '/experiences',
    '/skills',
    '/contacts',
    '/legals',
  ];
}

enum env {
  PROD,
  DEV,
}

enum contactType {
  URL,
  ID,
  OTHER,
}

enum skillType {
  LANGUAGE,
  STACK,
  OTHER,
}
