import 'package:url_launcher/url_launcher.dart';

class SharedFunctions {
  static Future<void> launchAnUrl(Uri url) async {
    if (!await launchUrl(url)) {
      throw 'Could not launch $url';
    }
  }
}
