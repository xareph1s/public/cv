import 'package:cv/cv/entities/experience.dart';
import 'package:cv/cv/repositories/experience_repository.dart';
import 'package:cv/shared/core/either_type.dart';
import 'package:cv/shared/errors/failures.dart';

class GetAllExperience {
  final ExperienceRepository repository;

  GetAllExperience(this.repository);

  Future<Either<Failure, List<Experience>>> call() async {
    return await repository.getAllExperience();
  }
}
