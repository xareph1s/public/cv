import 'package:cv/cv/entities/contact.dart';
import 'package:cv/cv/repositories/contact_repository.dart';
import 'package:cv/shared/core/either_type.dart';
import 'package:cv/shared/errors/failures.dart';

class GetAllContact {
  final ContactRepository repository;

  GetAllContact(this.repository);

  Future<Either<Failure, List<Contact>>> call() async {
    return await repository.getAllContact();
  }
}
