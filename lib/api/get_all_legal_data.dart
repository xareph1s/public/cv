import 'package:cv/cv/entities/legal.dart';
import 'package:cv/cv/repositories/legal_repository.dart';
import 'package:cv/shared/core/either_type.dart';
import 'package:cv/shared/errors/failures.dart';

class GetAllLegalData {
  final LegalRepository repository;

  GetAllLegalData(this.repository);

  Future<Either<Failure, List<Legal>>> call() async {
    return await repository.getAllLegalData();
  }
}
