import 'package:cv/cv/entities/skill.dart';
import 'package:cv/cv/repositories/skill_repository.dart';
import 'package:cv/shared/core/either_type.dart';
import 'package:cv/shared/errors/failures.dart';

class GetAllSkill {
  final SkillRepository repository;

  GetAllSkill(this.repository);

  Future<Either<Failure, List<Skill>>> call() async {
    return await repository.getAllSkill();
  }
}
