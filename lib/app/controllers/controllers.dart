import 'package:cv/api/get_all_contact.dart';
import 'package:cv/api/get_all_experience.dart';
import 'package:cv/api/get_all_legal_data.dart';
import 'package:cv/api/get_all_skill.dart';
import 'package:cv/cv/entities/contact.dart';
import 'package:cv/cv/entities/experience.dart';
import 'package:cv/cv/entities/legal.dart';
import 'package:cv/cv/entities/skill.dart';
import 'package:cv/cv/repositories/contact_local_data_source.dart';
import 'package:cv/cv/repositories/contact_repository.dart';
import 'package:cv/cv/repositories/experience_local_data_source.dart';
import 'package:cv/cv/repositories/experience_repository.dart';
import 'package:cv/cv/repositories/legal_local_data_source.dart';
import 'package:cv/cv/repositories/legal_repository.dart';
import 'package:cv/cv/repositories/skill_local_data_source.dart';
import 'package:cv/cv/repositories/skill_repository.dart';
import 'package:cv/shared/errors/failures.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

// Experience

final experienceLocalDataSourceProvider =
    Provider<ExperienceLocalDataSource>((ref) {
  return ExperienceLocalDataSourceImpl();
});

final experienceRepositoryProvider = Provider<ExperienceRepository>((ref) {
  return ExperienceRepositoryImpl(
      localDataSource: ref.watch(experienceLocalDataSourceProvider));
});

final experienceProvider = FutureProvider<List<Experience>>((ref) async {
  final res =
      await GetAllExperience(ref.watch(experienceRepositoryProvider)).call();
  return res.when(left: (e) => throw ServerFailure(), right: (list) => list);
});

// Skill

final skillLocalDataSourceProvider = Provider<SkillLocalDataSource>((ref) {
  return SkillLocalDataSourceImpl();
});

final skillRepositoryProvider = Provider<SkillRepository>((ref) {
  return SkillRepositoryImpl(
      localDataSource: ref.watch(skillLocalDataSourceProvider));
});

final skillProvider = FutureProvider<List<Skill>>((ref) async {
  final res = await GetAllSkill(ref.watch(skillRepositoryProvider)).call();
  return res.when(left: (e) => throw ServerFailure(), right: (list) => list);
});

// Contact

final contactLocalDataSourceProvider = Provider<ContactLocalDataSource>((ref) {
  return ContactLocalDataSourceImpl();
});

final contactRepositoryProvider = Provider<ContactRepository>((ref) {
  return ContactRepositoryImpl(
      localDataSource: ref.watch(contactLocalDataSourceProvider));
});

final contactProvider = FutureProvider<List<Contact>>((ref) async {
  final res = await GetAllContact(ref.watch(contactRepositoryProvider)).call();
  return res.when(left: (e) => throw ServerFailure(), right: (list) => list);
});

// Legal

final legalLocalDataSourceProvider = Provider<LegalLocalDataSource>((ref) {
  return LegalLocalDataSourceImpl();
});

final legalRepositoryProvider = Provider<LegalRepository>((ref) {
  return LegalRepositoryImpl(
      localDataSource: ref.watch(legalLocalDataSourceProvider));
});

final legalProvider = FutureProvider<List<Legal>>((ref) async {
  final res = await GetAllLegalData(ref.watch(legalRepositoryProvider)).call();
  return res.when(left: (e) => throw ServerFailure(), right: (list) => list);
});
