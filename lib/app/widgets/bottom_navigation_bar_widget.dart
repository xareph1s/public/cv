import 'package:cv/shared/core/constants.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class BottomNavigationBarWidget extends StatelessWidget {
  final int currentIndex;

  const BottomNavigationBarWidget({required this.currentIndex, super.key});

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: _bottomNavigationDestinationList(),
      currentIndex: currentIndex,
      backgroundColor: Colors.white,
      unselectedItemColor: Colors.black,
      unselectedLabelStyle: TextStyle(color: Colors.black),
      selectedItemColor: Colors.indigo,
      onTap: (int index) => GoRouter.of(context).go(Constants.pathUrl[index]),
    );
  }

  List<BottomNavigationBarItem> _bottomNavigationDestinationList() {
    return [
      BottomNavigationBarItem(
        icon: Icon(Icons.home),
        label: Constants.menuList[0],
        backgroundColor: Colors.white,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.cases_rounded),
        label: Constants.menuList[1],
        backgroundColor: Colors.white,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.code_rounded),
        label: Constants.menuList[2],
        backgroundColor: Colors.white,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.contacts),
        label: Constants.menuList[3],
        backgroundColor: Colors.white,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.flag),
        label: Constants.menuList[4],
        backgroundColor: Colors.white,
      ),
    ];
  }
}
