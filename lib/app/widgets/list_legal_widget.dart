import 'package:cv/cv/entities/legal.dart';
import 'package:flutter/material.dart';

class ListLegalWidget extends StatelessWidget {
  final List<Legal> list;

  const ListLegalWidget({super.key, required this.list});

  @override
  Widget build(BuildContext context) {
    List<Widget> res = [];
    for (Legal tmp in list) {
      Widget subRes = Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              tmp.name,
              style: TextStyle(fontSize: 20.0),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: Text(tmp.message),
            ),
          ],
        ),
      );
      res.add(subRes);
    }
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: res,
      ),
    );
  }
}
