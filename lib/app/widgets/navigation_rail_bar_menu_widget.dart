import 'package:cv/shared/core/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

class NavigationRailBarMenuWidget extends ConsumerWidget {
  final int currentIndex;

  const NavigationRailBarMenuWidget({required this.currentIndex, super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    double screenSize = MediaQuery.of(context).size.height - 200;
    return Row(
      children: [
        SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: screenSize),
            child: IntrinsicHeight(
              child: NavigationRail(
                destinations: navigationDestinationList(),
                selectedIndex: currentIndex,
                onDestinationSelected: (int index) {
                  GoRouter.of(context).go(Constants.pathUrl[index]);
                },
                labelType: NavigationRailLabelType.all,
                groupAlignment: 0,
              ),
            ),
          ),
        ),
        Container(
          color: Colors.white,
          child: VerticalDivider(),
        )
      ],
    );
  }

  List<NavigationRailDestination> navigationDestinationList() {
    return [
      NavigationRailDestination(
        icon: Icon(Icons.home),
        label: Text(Constants.menuList[0]),
      ),
      NavigationRailDestination(
        icon: Icon(Icons.cases_rounded),
        label: Text(Constants.menuList[1]),
      ),
      NavigationRailDestination(
        icon: Icon(Icons.code_rounded),
        label: Text(Constants.menuList[2]),
      ),
      NavigationRailDestination(
        icon: Icon(Icons.contacts),
        label: Text(Constants.menuList[3]),
      ),
      NavigationRailDestination(
        icon: Icon(Icons.flag),
        label: Text(Constants.menuList[4]),
      ),
    ];
  }
}
