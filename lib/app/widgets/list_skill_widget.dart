import 'package:cv/cv/entities/skill.dart';
import 'package:cv/shared/core/constants.dart';
import 'package:cv/shared/core/functions.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class ListSkillWidget extends StatelessWidget {
  final List<Skill> list;

  const ListSkillWidget({required this.list, super.key});

  @override
  Widget build(BuildContext context) {
    List<Widget> languages = [];
    List<Widget> stacks = [];
    List<Widget> others = [];
    List<Widget> res = [];

    for (Skill skill in list) {
      if (skill.type == skillType.LANGUAGE) {
        Widget tmp = _createSkill(skill, Colors.lightBlue);

        languages.add(tmp);
      } else if (skill.type == skillType.STACK) {
        Widget tmp = _createSkill(skill, Colors.green);

        stacks.add(tmp);
      } else {
        Widget tmp = _createSkill(skill, Colors.lime);

        others.add(tmp);
      }
    }

    res.add(_toSingleColumn("Langues", languages));
    res.add(_toSingleColumn("Technologies", stacks));
    res.add(_toSingleColumn("Autres", others));

    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: res,
      ),
    );
  }

  Widget _toSingleColumn(String title, List<Widget> list) {
    if (list.isNotEmpty) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(fontSize: 18.0),
            ),
            Column(
              children: list,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: Divider(thickness: 4.0),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  Widget _createSkill(Skill skill, Color color) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 10.0, 0, 5.0),
          child: Row(
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage(skill.logoUrl),
                maxRadius: 15.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: InkWell(
                  child: Text(skill.name),
                  onTap: (() =>
                      SharedFunctions.launchAnUrl(Uri.parse(skill.url))),
                ),
              ),
            ],
          ),
        ),
        LinearPercentIndicator(
          animation: true,
          animationDuration: 500,
          lineHeight: 20.0,
          percent: skill.progression,
          center: Text("${skill.progression * 100} %"),
          progressColor: color,
        ),
        (skill.description != null)
            ? Padding(
                padding: EdgeInsets.fromLTRB(10.0, 5.0, 0, 0),
                child: Text(skill.description!),
              )
            : Container(),
      ],
    );
  }
}
