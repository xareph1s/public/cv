import 'package:cv/shared/core/constants.dart';
import 'package:cv/shared/core/functions.dart';
import 'package:flutter/material.dart';

class CopyrightFooterWidget extends StatelessWidget {
  const CopyrightFooterWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Copyright "),
              Icon(Icons.copyright, size: 15.0),
              Text(
                  " ${DateTime.now().year} ${Constants.appName}. Tous droits réservés."),
            ],
          ),
          InkWell(
            child: Text("Version ${Constants.appVersion}"),
            onTap: (() =>
                SharedFunctions.launchAnUrl(Uri.parse(Constants.appCodeUrl))),
          ),
        ],
      ),
    );
  }
}
