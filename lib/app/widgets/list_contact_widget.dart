import 'package:cv/cv/entities/contact.dart';
import 'package:cv/shared/core/constants.dart';
import 'package:cv/shared/core/functions.dart';
import 'package:flutter/material.dart';

class ListContactWidget extends StatelessWidget {
  final List<Contact> list;
  final bool isSmartphoneSize;

  const ListContactWidget(
      {required this.list, required this.isSmartphoneSize, super.key});

  @override
  Widget build(BuildContext context) {
    List<Widget> urls = [];
    List<Widget> ids = [];
    List<Widget> others = [];

    for (Contact contact in list) {
      if (contact.type == contactType.URL) {
        Widget tmp = _createContact(context, contact, "URL");

        urls.add(tmp);
      } else if (contact.type == contactType.ID) {
        Widget tmp = _createContact(context, contact, "ID");

        ids.add(tmp);
      } else {
        Widget tmp = _createContact(context, contact, "OTHER");

        others.add(tmp);
      }
    }

    urls.addAll(ids);
    urls.addAll(others);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: urls,
    );
  }

  Widget _createContact(BuildContext context, Contact contact, String type) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          SizedBox(
            width: isSmartphoneSize
                ? MediaQuery.of(context).size.width
                : MediaQuery.of(context).size.width / 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    CircleAvatar(
                      backgroundImage: NetworkImage(contact.imageUrl),
                      maxRadius: 15.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Text(contact.name),
                    ),
                  ],
                ),
                contact.type == contactType.URL
                    ? _urlWidget(contact.information)
                    : Text(contact.information)
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _urlWidget(String url) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0),
      child: InkWell(
        child: Text("Lien"),
        onTap: (() => SharedFunctions.launchAnUrl(Uri.parse(url))),
      ),
    );
  }
}
