import 'package:cv/cv/entities/experience.dart';
import 'package:cv/shared/core/functions.dart';
import 'package:flutter/material.dart';

class ListExperienceWidget extends StatelessWidget {
  final List<Experience> list;

  const ListExperienceWidget({Key? key, required this.list}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> res = [];

    for (Experience exp in list) {
      Widget tmp = Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        exp.name,
                        style: TextStyle(
                          fontSize: 25.0,
                        ),
                      ),
                    ),
                    Text(
                        "${exp.startTime.year} - ${_endTimeOrNot(exp.endTime)}"),
                  ],
                ),
                Row(
                  children: [
                    exp.logo.isNotEmpty
                        ? CircleAvatar(
                            backgroundImage: NetworkImage(exp.logo),
                            maxRadius: 15.0,
                          )
                        : Container(),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: InkWell(
                        child: Text(exp.companyName),
                        onTap: (() => exp.url.isNotEmpty
                            ? SharedFunctions.launchAnUrl(Uri.parse(exp.url))
                            : Container()),
                      ),
                    ),
                  ],
                ),
                Divider(),
              ],
            ),
            exp.description.isNotEmpty
                ? Text(exp.description, style: TextStyle(fontSize: 16.0))
                : Container(),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Text(
                _listMission(exp.missions),
              ),
            ),
          ],
        ),
      );

      res.add(tmp);
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: res,
    );
  }

  String _listMission(List<String> l) {
    String res = "";
    for (String val in l) {
      res += "* $val\n";
    }
    return res;
  }

  String _endTimeOrNot(DateTime? date) {
    if (date == null) {
      return "Présent";
    } else {
      return date.year.toString();
    }
  }
}
