import 'package:cv/app/widgets/bottom_navigation_bar_widget.dart';
import 'package:cv/app/widgets/copyright_footer_widget.dart';
import 'package:cv/app/widgets/navigation_rail_bar_menu_widget.dart';
import 'package:cv/shared/core/constants.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxHeight < constraints.maxWidth) {
            return SizedBox(
              width: Constants.maxContainerSize.toDouble(),
              height: MediaQuery.of(context).size.height - 200,
              child: Row(
                children: [
                  NavigationRailBarMenuWidget(
                    currentIndex: 0,
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.white,
                      height: MediaQuery.of(context).size.height,
                      child: SelectionArea(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: [
                              Expanded(
                                child: Container(),
                              ),
                              Text(
                                "Bienvenue sur le site de ${Constants.appName}",
                                style: TextStyle(fontSize: 30.0),
                                textAlign: TextAlign.center,
                              ),
                              Expanded(
                                child: Container(),
                              ),
                              CopyrightFooterWidget(),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    color: Colors.white,
                  ),
                ),
                Container(
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width,
                  child: SelectionArea(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        "Bienvenue sur le site d'${Constants.appName}",
                        style: TextStyle(fontSize: 30.0),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    color: Colors.white,
                  ),
                ),
                BottomNavigationBarWidget(currentIndex: 0),
              ],
            );
          }
        }),
      ),
    );
  }
}
