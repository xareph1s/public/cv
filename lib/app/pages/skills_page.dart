import 'package:cv/app/controllers/controllers.dart';
import 'package:cv/app/widgets/bottom_navigation_bar_widget.dart';
import 'package:cv/app/widgets/copyright_footer_widget.dart';
import 'package:cv/app/widgets/list_skill_widget.dart';
import 'package:cv/app/widgets/loading_widget.dart';
import 'package:cv/app/widgets/navigation_rail_bar_menu_widget.dart';
import 'package:cv/shared/core/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SkillsPage extends ConsumerWidget {
  const SkillsPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var skillDataProvider = ref.watch(skillProvider);

    return Scaffold(
      body: Center(
        child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxHeight < constraints.maxWidth) {
            return SizedBox(
              width: Constants.maxContainerSize.toDouble(),
              height: MediaQuery.of(context).size.height - 200,
              child: Row(
                children: [
                  NavigationRailBarMenuWidget(
                    currentIndex: 2,
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.white,
                      height: MediaQuery.of(context).size.height,
                      child: SelectionArea(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: [
                              Expanded(
                                child: ListView(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: skillDataProvider.hasValue
                                          ? ListSkillWidget(
                                              list: skillDataProvider.value!)
                                          : LoadingWidget(),
                                    ),
                                  ],
                                ),
                              ),
                              CopyrightFooterWidget(),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    color: Colors.white,
                    width: MediaQuery.of(context).size.width,
                    child: SelectionArea(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ListView(
                          children: [
                            skillDataProvider.hasValue
                                ? ListSkillWidget(
                                    list: skillDataProvider.value!,
                                  )
                                : LoadingWidget(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                BottomNavigationBarWidget(currentIndex: 2),
              ],
            );
          }
        }),
      ),
    );
  }
}
