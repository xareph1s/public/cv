import 'package:cv/app/pages/contacts_page.dart';
import 'package:cv/app/pages/experiences.page.dart';
import 'package:cv/app/pages/home_page.dart';
import 'package:cv/app/pages/legals_page.dart';
import 'package:cv/app/pages/skills_page.dart';
import 'package:cv/app/pages/test_page.dart';
import 'package:cv/shared/core/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

void main() {
  runApp(ProviderScope(child: App()));
}

class App extends StatelessWidget {
  App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: _router,
      debugShowCheckedModeBanner: false,
      title: Constants.appName,
      theme: ThemeData(
        primarySwatch: Colors.indigo,
        useMaterial3: true,
      ),
    );
  }

  final GoRouter _router = GoRouter(routes: <GoRoute>[
    GoRoute(
      path: Constants.pathUrl[0],
      pageBuilder: (BuildContext context, GoRouterState state) {
        return NoTransitionPage(
          child: SafeArea(
            child: (Constants.currentEnv == env.PROD)
                ? const HomePage()
                : const TestPage(),
          ),
        );
      },
    ),
    GoRoute(
      path: Constants.pathUrl[1],
      pageBuilder: ((BuildContext context, GoRouterState state) =>
          NoTransitionPage(
              child: SafeArea(
            child: ExperiencesPage(),
          ))),
    ),
    GoRoute(
      path: Constants.pathUrl[2],
      pageBuilder: ((BuildContext context, GoRouterState state) =>
          NoTransitionPage(
            child: SafeArea(
              child: SkillsPage(),
            ),
          )),
    ),
    GoRoute(
      path: Constants.pathUrl[3],
      pageBuilder: ((BuildContext context, GoRouterState state) =>
          NoTransitionPage(
            child: SafeArea(
              child: ContactsPage(),
            ),
          )),
    ),
    GoRoute(
      path: Constants.pathUrl[4],
      pageBuilder: ((BuildContext context, GoRouterState state) =>
          NoTransitionPage(
            child: SafeArea(
              child: LegalsPage(),
            ),
          )),
    ),
  ]);
}
