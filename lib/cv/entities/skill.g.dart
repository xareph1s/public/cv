// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'skill.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Skill _$$_SkillFromJson(Map<String, dynamic> json) => _$_Skill(
      type: $enumDecode(_$skillTypeEnumMap, json['type']),
      logoUrl: json['logoUrl'] as String,
      name: json['name'] as String,
      url: json['url'] as String,
      description: json['description'] as String?,
      progression: (json['progression'] as num).toDouble(),
    );

Map<String, dynamic> _$$_SkillToJson(_$_Skill instance) => <String, dynamic>{
      'type': _$skillTypeEnumMap[instance.type]!,
      'logoUrl': instance.logoUrl,
      'name': instance.name,
      'url': instance.url,
      'description': instance.description,
      'progression': instance.progression,
    };

const _$skillTypeEnumMap = {
  skillType.LANGUAGE: 'LANGUAGE',
  skillType.STACK: 'STACK',
  skillType.OTHER: 'OTHER',
};
