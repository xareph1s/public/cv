// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'legal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Legal _$$_LegalFromJson(Map<String, dynamic> json) => _$_Legal(
      name: json['name'] as String,
      message: json['message'] as String,
    );

Map<String, dynamic> _$$_LegalToJson(_$_Legal instance) => <String, dynamic>{
      'name': instance.name,
      'message': instance.message,
    };
