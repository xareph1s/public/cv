// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Contact _$$_ContactFromJson(Map<String, dynamic> json) => _$_Contact(
      imageUrl: json['imageUrl'] as String,
      name: json['name'] as String,
      type: $enumDecode(_$contactTypeEnumMap, json['type']),
      information: json['information'] as String,
    );

Map<String, dynamic> _$$_ContactToJson(_$_Contact instance) =>
    <String, dynamic>{
      'imageUrl': instance.imageUrl,
      'name': instance.name,
      'type': _$contactTypeEnumMap[instance.type]!,
      'information': instance.information,
    };

const _$contactTypeEnumMap = {
  contactType.URL: 'URL',
  contactType.ID: 'ID',
  contactType.OTHER: 'OTHER',
};
