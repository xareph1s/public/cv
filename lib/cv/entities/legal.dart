import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'legal.freezed.dart';
part 'legal.g.dart';

@freezed
class Legal with _$Legal {
  const factory Legal({
    required String name,
    required String message,
  }) = _Legal;

  factory Legal.fromJson(Map<String, Object?> json) => _$LegalFromJson(json);
}
