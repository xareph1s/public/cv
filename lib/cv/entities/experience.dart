import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'experience.freezed.dart';
part 'experience.g.dart';

@freezed
class Experience with _$Experience {
  const factory Experience({
    required String logo,
    required String companyName,
    required String url,
    required String name,
    required String description,
    required DateTime startTime,
    required DateTime? endTime,
    required List<String> missions,
  }) = _Experience;

  factory Experience.fromJson(Map<String, Object?> json) =>
      _$ExperienceFromJson(json);
}
