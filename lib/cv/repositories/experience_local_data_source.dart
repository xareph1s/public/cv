import 'dart:convert';
import 'package:cv/cv/entities/experience.dart';
import 'package:cv/shared/core/constants.dart';
import 'package:cv/shared/errors/exception.dart';
import 'package:flutter/services.dart';

abstract class ExperienceLocalDataSource {
  /// Throws a [ServerException] for all error codes.
  Future<List<Experience>> getAllExperience();
}

class ExperienceLocalDataSourceImpl implements ExperienceLocalDataSource {
  @override
  Future<List<Experience>> getAllExperience() async {
    try {
      final String response =
          await rootBundle.loadString(Constants.experienceDataLocation);
      final data = json.decode(response);
      final List<Experience> list = List<Experience>.from(
          data.map((model) => Experience.fromJson(model)));
      return list;
    } catch (e) {
      throw ServerException();
    }
  }
}
