import 'dart:convert';
import 'package:cv/cv/entities/legal.dart';
import 'package:cv/shared/core/constants.dart';
import 'package:cv/shared/errors/exception.dart';
import 'package:flutter/services.dart';

abstract class LegalLocalDataSource {
  /// Throws a [ServerException] for all error codes.
  Future<List<Legal>> getAllLegalData();
}

class LegalLocalDataSourceImpl implements LegalLocalDataSource {
  @override
  Future<List<Legal>> getAllLegalData() async {
    try {
      final String response =
          await rootBundle.loadString(Constants.legalDataLocation);
      final data = json.decode(response);
      final List<Legal> list =
          List<Legal>.from(data.map((model) => Legal.fromJson(model)));
      return list;
    } catch (e) {
      throw ServerException();
    }
  }
}
