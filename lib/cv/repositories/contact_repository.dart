import 'package:cv/cv/entities/contact.dart';
import 'package:cv/cv/repositories/contact_local_data_source.dart';
import 'package:cv/shared/core/either_type.dart';
import 'package:cv/shared/errors/exception.dart';
import 'package:cv/shared/errors/failures.dart';

abstract class ContactRepository {
  Future<Either<Failure, List<Contact>>> getAllContact();
}

class ContactRepositoryImpl implements ContactRepository {
  final ContactLocalDataSource localDataSource;

  ContactRepositoryImpl({
    required this.localDataSource,
  });

  @override
  Future<Either<Failure, List<Contact>>> getAllContact() async {
    try {
      return Right(await localDataSource.getAllContact());
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
