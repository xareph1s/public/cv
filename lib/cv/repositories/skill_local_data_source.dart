import 'dart:convert';
import 'package:cv/cv/entities/skill.dart';
import 'package:cv/shared/core/constants.dart';
import 'package:cv/shared/errors/exception.dart';
import 'package:flutter/services.dart';

abstract class SkillLocalDataSource {
  /// Throws a [ServerException] for all error codes.
  Future<List<Skill>> getAllSkill();
}

class SkillLocalDataSourceImpl implements SkillLocalDataSource {
  @override
  Future<List<Skill>> getAllSkill() async {
    try {
      final String response =
          await rootBundle.loadString(Constants.skillDataLocation);
      final data = json.decode(response);
      final List<Skill> list =
          List<Skill>.from(data.map((model) => Skill.fromJson(model)));
      return list;
    } catch (e) {
      throw ServerException();
    }
  }
}
