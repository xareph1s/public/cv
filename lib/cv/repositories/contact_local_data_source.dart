import 'dart:convert';
import 'package:cv/cv/entities/contact.dart';
import 'package:cv/shared/core/constants.dart';
import 'package:cv/shared/errors/exception.dart';
import 'package:flutter/services.dart';

abstract class ContactLocalDataSource {
  /// Throws a [ServerException] for all error codes.
  Future<List<Contact>> getAllContact();
}

class ContactLocalDataSourceImpl implements ContactLocalDataSource {
  @override
  Future<List<Contact>> getAllContact() async {
    try {
      final String response =
          await rootBundle.loadString(Constants.contactDataLocation);
      final data = json.decode(response);
      final List<Contact> list =
          List<Contact>.from(data.map((model) => Contact.fromJson(model)));
      return list;
    } catch (e) {
      throw ServerException();
    }
  }
}
