import 'package:cv/cv/entities/legal.dart';
import 'package:cv/cv/repositories/legal_local_data_source.dart';
import 'package:cv/shared/core/either_type.dart';
import 'package:cv/shared/errors/exception.dart';
import 'package:cv/shared/errors/failures.dart';

abstract class LegalRepository {
  Future<Either<Failure, List<Legal>>> getAllLegalData();
}

class LegalRepositoryImpl implements LegalRepository {
  final LegalLocalDataSource localDataSource;

  LegalRepositoryImpl({
    required this.localDataSource,
  });

  @override
  Future<Either<Failure, List<Legal>>> getAllLegalData() async {
    try {
      return Right(await localDataSource.getAllLegalData());
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
