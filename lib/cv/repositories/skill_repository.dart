import 'package:cv/cv/entities/skill.dart';
import 'package:cv/cv/repositories/skill_local_data_source.dart';
import 'package:cv/shared/core/either_type.dart';
import 'package:cv/shared/errors/exception.dart';
import 'package:cv/shared/errors/failures.dart';

abstract class SkillRepository {
  Future<Either<Failure, List<Skill>>> getAllSkill();
}

class SkillRepositoryImpl implements SkillRepository {
  final SkillLocalDataSource localDataSource;

  SkillRepositoryImpl({
    required this.localDataSource,
  });

  @override
  Future<Either<Failure, List<Skill>>> getAllSkill() async {
    try {
      return Right(await localDataSource.getAllSkill());
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
