import 'package:cv/cv/entities/experience.dart';
import 'package:cv/cv/repositories/experience_local_data_source.dart';
import 'package:cv/shared/core/either_type.dart';
import 'package:cv/shared/errors/exception.dart';
import 'package:cv/shared/errors/failures.dart';

abstract class ExperienceRepository {
  Future<Either<Failure, List<Experience>>> getAllExperience();
}

class ExperienceRepositoryImpl implements ExperienceRepository {
  final ExperienceLocalDataSource localDataSource;

  ExperienceRepositoryImpl({
    required this.localDataSource,
  });

  @override
  Future<Either<Failure, List<Experience>>> getAllExperience() async {
    try {
      return Right(await localDataSource.getAllExperience());
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
